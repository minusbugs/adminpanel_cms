<?php
namespace App\Models;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model implements Authenticatable {
	use AuthenticableTrait;
	protected $table = 'login';
	protected $primaryKey = 'Id';
	protected $fillable = array('Id');
}