<?php

namespace App\Http\Controllers;
use App\Models\Employee;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;



class Employees extends Controller
{
    private $request;
    /**
     * Create a new authentication controller instance.
     * @return void
     */

    public function __construct(Request $request) {
        $this->request = $request;

    }
    //
    public function logincheck()
    {
        //echo "login";
        $username = $this->request->get('uname');
        $password = $this->request->get('password');
        $user = Employee::where('UserName', $username)
                    ->where('PassWord',$password)
                    ->get();

        if($user->isEmpty()){
            $responsearr = [
            'resultcode' =>2,
            'userdetails'=>$user
            ];
            //echo json_encode($user);
            return response()->json($responsearr);
        }
        else
        {
            $this->request->session()->put('sessionname', $username);
            $responsearr =[
            'resultcode' =>1,
            'userdetails' =>$username
            ];

            return response()->json($responsearr,200);
            //echo json_encode($username);
        }
    }

    public function addgallerydetails(Request $request)
    {
        $gallery = new Gallery();
        $file    = $request->file('galleryimage');
        $filename = $file->getClientOriginalName();
        $destinationpath = basepath().'/public/galleryimages';
        $file->move($destinationpath,$filename);
        $gallery ->GalleryId        = 1;
        $gallery ->ImageTitle       = $request->get('imagetitle');
        $gallery ->ImageDescription = $fileName;
        $gallery ->ImageUrl         = $request->get('imagedescription');
        $result                     = $gallery->save();
        return json_encode($result);


    }

     public function postImage(Request $request)
    {
        $this->validate($request, [
            'image_file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        $imageName = time().'.'.$request->image_file->getClientOriginalExtension();
        $request->image_file->move(public_path('images'), $imageName);
        return back()
            ->with('success','You have successfully upload images.')
            ->with('image',$imageName);
    }
    public function add() {
    
        $file = Request::file('filefield');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
        $entry = new Fileentry();
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $file->getFilename().'.'.$extension;
 
        $entry->save();
 
        return redirect('fileentry');
        
    }
}
