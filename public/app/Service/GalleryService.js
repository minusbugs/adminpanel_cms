angular.module('admingal').factory('GalleryService',['$http',function($http){
	var GalleryService = {
		addgallery : function(imagetitle,imagedescription){
			return $http.post('http://127.0.0.1:8000/addgallerydetails',{imagetitle:imagetitle,imagedescription:imagedescription});
		}
	}
	return GalleryService;
}]);