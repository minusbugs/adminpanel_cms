<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('login');
});

Route::post('/login','Employees@logincheck');

Route::get('/addgallery', function () {
    return view('gallery');
});

Route::post('/addgallerydetails','Employees@addgallerydetails');
//Route::post('image-upload','Employees@postImage');

// Route::any('image-upload', function(){
//  return Input::file('file')->move(__DIR__.'/storage/',Input::file('file')->getClientOriginalName());
// });

// Route::post('add','Employees@postImage');

