</section>
 <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{ asset("assets/scripts/modernizr.js") }}"></script>
<script src="{{ asset("assets/lib/jquery/jquery.js") }}""></script>
<script src="{{ asset("assets/lib/jquery-ui/jquery-ui.js") }}"></script>
<script src="{{ asset("assets/lib/bootstrap/js/bootstrap.js") }}"></script>
<script src="{{ asset("assets/lib/jquery-autosize/autosize.js") }}"></script>
<script src="{{ asset("assets/lib/select2/select2.js") }}"></script>
<script src="{{ asset("assets/lib/jquery-toggles/toggles.js") }}"></script>
<script src="{{ asset("assets/lib/jquery-maskedinput/jquery.maskedinput.js") }}"></script>
<script src="{{ asset("assets/lib/timepicker/jquery.timepicker.js") }}"></script>
<!-- <script src="{{ asset("assets/lib/dropzone/dropzone.js") }}"></script> -->
<script src="{{ asset("assets/lib/bootstrapcolorpicker/js/bootstrap-colorpicker.js") }}"></script>

<script src="{{ asset("assets/scripts/quirk.js") }}"></script>

<script>
$(document).ready(function() {
                $('input[type="file"]').imageuploadify();
            })
$(function() {

  // Textarea Auto Resize
  autosize($('#autosize'));

  // Select2 Box
  $('#select1, #select2, #select3').select2();
  $("#select4").select2({ maximumSelectionLength: 2 });
  $("#select5").select2({ minimumResultsForSearch: Infinity });
  $("#select6").select2({ tags: true });

  // Toggles
  $('.toggle').toggles({
    on: true,
    height: 26
  });

  // Input Masks
  $("#date").mask("99/99/9999");
  $("#phone").mask("(999) 999-9999");
  $("#ssn").mask("999-99-9999");

  // Date Picker
  $('#datepicker').datepicker();
  $('#datepicker-inline').datepicker();
  $('#datepicker-multiple').datepicker({ numberOfMonths: 2 });

  // Time Picker
  $('#tpBasic').timepicker();
  $('#tp2').timepicker({'scrollDefault': 'now'});
  $('#tp3').timepicker();

  $('#setTimeButton').on('click', function (){
    $('#tp3').timepicker('setTime', new Date());
  });

  // Colorpicker
  $('#colorpicker1').colorpicker();
  $('#colorpicker2').colorpicker({
    customClass: 'colorpicker-lg',
    sliders: {
      saturation: {
        maxLeft: 200,
        maxTop: 200
      },
      hue: { maxTop: 200 },
      alpha: { maxTop: 200 }
    }
  });

});
</script>

</body>
</html>
