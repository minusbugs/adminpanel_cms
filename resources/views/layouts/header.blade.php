<!DOCTYPE html>
<html lang="en" ng-app="admingal">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

  <title>Laravel</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset("assets/lib/jquery-ui/jquery-ui.css") }}" >
  <!-- <link rel="stylesheet" href="{{ asset("assets/lib/select2/select2.css") }}" > -->
 <!--  <link rel="stylesheet" href="{{ asset("assets/lib/dropzone/dropzone.css") }}" > -->
  <link rel="stylesheet" href="{{ asset("assets/lib/jquery-toggles/toggles-full.css") }}">
  <link rel="stylesheet" href="{{ asset("assets/lib/fontawesome/css/font-awesome.css") }}">
  <link rel="stylesheet" href="{{ asset("assets/lib/timepicker/jquery.timepicker.css") }}">
  <link rel="stylesheet" href="{{ asset("assets/lib/bootstrapcolorpicker/css/bootstrap-colorpicker.css") }}">

  <link rel="stylesheet" href="{{ asset("assets/stylesheets/quirk.css") }}">
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/imageuploadify.min.css") }}">
  <link rel="stylesheet" href="{{ asset("assets/stylesheets/main.css") }}">
  
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.js"></script>
  <script src="{{ asset("assets/scripts/modernizr.js") }}"></script>
  <script type="text/javascript" src="{{ asset("assets/scripts/imageuploadify.min.js") }}"></script>
  <script src="{{ asset("app/app.js") }}" type="text/javascript"></script>
  <script src="{{ asset("app/Controllers/GalleryController.js") }}" type="text/javascript"></script>
  <script src="{{ asset("app/Service/GalleryService.js") }}" type="text/javascript"></script>
</head>

<body>

  <header>
    <div class="headerpanel">

      <div class="logopanel">
        <h2><a href="index.html">Quirk</a></h2>
      </div><!-- logopanel -->

      <div class="headerbar">

        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

        <div class="header-right">
        <ul class="headermenu">
          <li>
            <a href="{{ url ('') }}"><button id="chatview" class="btn btn-chat alert-notice">
             <!--  <span class="badge-alert"></span> -->
              <i class="fa fa-sign-out"></i>
            </button></a>
          </li>
        </ul>
      </div><!-- header-right -->
    </div><!-- headerbar -->
  </div><!-- header-->
</header>

<section>

  <div class="leftpanel">
    <div class="leftpanelinner">
      <div class="tab-content">

        <!-- ################# MAIN MENU ################### -->

        <div class="tab-pane active" id="mainmenu">
         <!--  <h5 class="sidebar-title">Favorites</h5> -->
         <h5 class="sidebar-title">Main Menu</h5>
          <ul class="nav nav-pills nav-stacked nav-quirk" id="ul_dashboard">
            <li><a href="index.html"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
            <!-- <li><a href="widgets.html"><span class="badge pull-right">10+</span><i class="fa fa-cube"></i> <span>Widgets</span></a></li>
            <li><a href="maps.html"><i class="fa fa-map-marker"></i> <span>Maps</span></a></li> -->
          </ul>
          <ul class="nav nav-pills nav-stacked nav-quirk">
            <li class="nav-parent active">
              <a href=""><i class="fa fa-check-square"></i> <span>Media</span></a>
              <ul class="children">
                <li  id="li_viewgallery"><a href="viewgallery.html">View Gallery</a></li>
                <li id="li_addgallery"><a href="{{ url ('/addgallery') }}">Add Gallery</a></li>
               <!--  <li><a href="form-wizards.html">Form Wizards</a></li>
                <li><a href="wysiwyg.html">Text Editor</a></li> -->
              </ul>
            </li>
            </ul>
        </div><!-- tab-pane -->

      </div><!-- tab-content -->

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->