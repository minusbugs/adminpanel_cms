@extends('layouts.header')
<script type="text/javascript">
   window.onload = function() {
       $(".se-pre-con").fadeOut("slow");
     $('#li_addgallery').removeClass('active');
     document.getElementById('li_addgallery').className = 'active';
   };
</script>
<div class="mainpanel"  ng-controller="GalleryController">
   <div class="contentpanel">
      <ol class="breadcrumb breadcrumb-quirk">
         <li><a href="index.html"><i class="fa fa-home mr5"></i> Home</a></li>
         <li><a>Media</a></li>
         <li class="active">Add Gallery</li>
      </ol>
      <div class="row">
      </div>
      <!-- col-sm-6 -->
      <!-- ####################################################### -->
      <div class="col-sm-12">
         <div class="panel">
            <div class="panel-heading">
               <h4 class="panel-title">Add Gallery Images</h4>
            </div>
            <div class="panel-body">
               <form>
                  <div class="row pad">
                     <div class="col-md-2">
                        <label>Image Title <span class="text-danger">*</span></label>
                     </div>
                     <div class="col-sm-8">
                        <div class="form-group">
                           <input type="text" name="txttitle" id="txttitle" maxlength="60" placeholder="Title"  class="form-control" />
                        </div>
                     </div>
                  </div>
                  <div class="row pad">
                     <div class="col-md-2">
                        <label>Image Description <span class="text-danger">*</span></label>
                     </div>
                     <div class="col-sm-8">
                        <div class="form-group">
                           <input type="text" name="txtdescription" id="txtdescription" maxlength="60" placeholder="Title"  class="form-control" />
                        </div>
                     </div>
                  </div>
                  <div class="row pad">
                     <div class="col-md-2">
                        <label>Image url <span class="text-danger">*</span></label>
                     </div>
                     <div class="col-sm-8">
                        <div class="form-group">
                           <input type="file" id="galleryimage" name="galleryimage" accept="image/*,audio/*,video/*" multiple>
                           <!-- <input name="file" type="file" multiple /> -->
                        </div>
                     </div>
                  </div>
                  <div class="panel-body paddingtop10">
                     <button class="btn btn-success btn-quirk btn-stroke" type="submit" ng-click="Gallery.LoginCheck()">Upload</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!-- col-sm-6 -->
   </div>
   <!-- row -->
</div>
<!-- contentpanel -->
</div><!-- mainpanel -->
@extends('layouts.footer')