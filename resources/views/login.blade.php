<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js" ng-app="admingal">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Laravel </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link rel="stylesheet" href="{{ asset("assets/stylesheets/font-awesome.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/stylesheets/quirk.css") }}">
    <!-- <link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" /> -->
     <link href="{{ asset("assets/stylesheets/toastr.css") }}" rel="stylesheet"/> 


</head>
<body class="signwrapper">
    @yield('body')
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
    <script src="{{ asset("assets/scripts/modernizr.js") }}"></script>
    <script src="{{ asset("assets/scripts/toastr.min.js") }}" type="text/javascript"></script> 
    <script src="{{ asset("app/app.js") }}" type="text/javascript"></script>
    <script src="{{ asset("app/Controllers/LoginController.js") }}" type="text/javascript"></script>
    <script src="{{ asset("app/Service/LoginService.js") }}" type="text/javascript"></script>
    
<!-- </body>
</html> -->

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <div class="panel signin" ng-controller="LoginController">
    <div class="panel-heading">
      <h1>Quirk</h1>
      <h4 class="panel-title">Welcome! Please signin.</h4>
    </div>
    <div class="panel-body">
     <!--  <button class="btn btn-primary btn-quirk btn-fb btn-block">Connect with Facebook</button> -->
      <!-- <div class="or">or</div> -->
      <form role="form" ng-submit="Login.LoginCheck()" method="post">
        <div class="form-group mb10">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" class="form-control" placeholder="Enter Username" id="username">
          </div>
        </div>
        <div class="form-group nomargin">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="text" class="form-control" placeholder="Enter Password" id="password">
          </div>
        </div>
        <div><a href="" class="forgot"></a></div>
        <div class="form-group">
          <button class="btn btn-success btn-quirk btn-block" type="submit">Sign In</button>
        </div>
      </form>
      <hr class="invisible">
     <!--  <div class="form-group">
        <a href="signup.html" class="btn btn-default btn-quirk btn-stroke btn-stroke-thin btn-block btn-sign">Not a member? Sign up now!</a>
      </div> -->
    </div>
  </div><!-- panel -->
</body>
</html>